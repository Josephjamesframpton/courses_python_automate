#!/bin/bash
# before and after tax calculator
import sys
# alberta rates
alberta_rates = [
    {'rate': 0.15, 'range': 46605},
    {'rate': 0.205, 'range' : 46603},
    {'rate': 0.26, 'range' : 51281},
    {'rate': 0.29, 'range' : 61353},
    {'rate': 0.33, 'range' : 205842}
    ]


total = 0
# not in the right order may need an array
for val in alberta_rates:
    print(val)
    val['break_point'] = val['range'] + total
    total += val['range']
    print(val)
# this could be done recursively
income = int(sys.argv[1])
income_out = 0
# start at the end of the list and work backwards
alberta_rates.reverse()
for num, val in enumerate(alberta_rates):
#    if income <= val['break_point'] and income >= (val['break_point'] - val['range']):
    # dont use the range for 5th bracket
    print(num)
    print(val)
    if income >= val['range'] and num == 0:
        income_out += (income - val['range']) * val['rate']
        income = val['range']
        print(income)
print(income_out)
print(income)

# if income <= alberta_rates['bracket1']['range']:
#     # calculate
# elif income <= alberta_rates['bracket2']['range']:
#     print()
# elif income <= alberta_rates['bracket3']['range']:
#     print()
# elif income <= alberta_rates['bracket4']['range']:
#     print()
# elif income <= alberta_rates['bracket5']['range']:
#     print()
