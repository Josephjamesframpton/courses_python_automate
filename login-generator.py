#!/bin/bash

# password manager (not secure)

PASSWORDS = {'gmail': 'gmail password',
        'beanstalk': 'beans password'
        }

import sys, pyperclip
if len(sys.argv) < 2:
    print('Usage: python pw.py [account] - copy account password')
    sys.exit()

account = sys.argv[1]

if account in PASSWORDS:
    pyperclip.copy(PASSWORDS[account])
    print('password for ' + account + ' copied to clip board')
else:
    print('There is no account named ' + account)
