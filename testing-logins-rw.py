#!/bin/bash
# testing read / write
import json

# file_handle = open('/Users/josephframpton/Documents/code/python-automate-boring/logins.txt', 'r+')

# print(file_handle.read())
# print(file_handle.readline())
# print(file_handle.readlines())
# print(file_handle.write('\n is this working?'))
# file_handle.close()

# or use other syntax with no need to close file:
# with open(“hello.txt”, “w”) as f:
# f.write(“Hello World”)

with open('/Users/josephframpton/Documents/code/python-automate-boring/logins.txt', 'r+') as data_file:
    data = json.load(data_file)

print(data)
