#!/bin/bash
# password manager (not secure)
# NOTE: current issue
# can encode password and decoded
# if saved after encoded, can not decode (problem with json conversion using non-ascii characters??)
import sys, pyperclip, hashlib, json, base64, os
from Crypto import Random
from Crypto.Cipher import AES

# key = 'abcdefghijklmnop'
# login_path = './logins.txt'
# login_path = '/Users/josephframpton/Documents/code/python-automate-boring/logins.txt'
login_path = os.path.join(os.path.dirname(__file__),'logins.txt')

def get_key(application_name):
    key = hashlib.sha1(application_name.encode())
    key = key.hexdigest()
    key = key[:16]
    return key

def encrypt(data, key):
    iv_aes = Random.new().read(AES.block_size) # salt?
    # print('iv_aes : ')
    # print(iv_aes)
    cipher_aes = AES.new(key.encode(), AES.MODE_CFB, iv_aes) # key
    # print('cipher_aes : ')
    # print(cipher_aes)
    encrypted_aes = cipher_aes.encrypt(data.encode()) # encription
    print("before encryptioin")
    print(iv_aes + encrypted_aes)
    result = base64.b64encode(iv_aes + encrypted_aes).decode('utf-8')
    print("this is the first base64")
    print(result)
    return result

def decrypt(encrypted_aes, key):
    # print('result passed : ')
    print("this si the second base64")
    print(encrypted_aes)
    decoded_base64 = base64.b64decode(encrypted_aes)
    print("this is the decrypted base 64")
    print(decoded_base64)
    iv_aes = decoded_base64[:AES.block_size]
    # print('iv_aes : ')
    # print(iv_aes)
    dec_cipher_aes = AES.new(key.encode(), AES.MODE_CFB, iv_aes)
    # print('dec_cipher_aes : ')
    # print(dec_cipher_aes)
    something = decoded_base64[AES.block_size:]
    # print("something")
    # print(something)
    return dec_cipher_aes.decrypt(something).decode('utf-8')

#input
# message = 'password'
# #first 16 chars from sha1 of account name
# key = '1234567890123456'
# encrypted = {}
# # encryption
# encrypt(message, key)
# # decrypt
# decrypt(key, encrypted['iv_aes'], encrypted['encrypted_aes'])

# NOTE: need to install:
# pyperclip
# pycrypto
# json

# needs to store passwords as hash to be secure
#   this means passwords will need to be reset in all applications to take a hash code
#   could also MD5 them encrypt to save, decrypt before using
#       this is less secure but better than storing plain text passwords somewhere

# to use at work:
# bean stalk
# kanban
# gmail
# hip chat
# production servers
# test1
# staging site

with open(login_path, 'r') as data_file:
    data = json.load(data_file)
ACCOUNTS = data

ACCOUNT_LABELS = ['accounts', 'account', 'acc', 'ac', 'a', 'current', 'curr', 'cur', 'c']
NOTE_LABELS = ['note', 'notes', 'nt', 'n']

# ENCRYPTION - successful

testHash = 'password'
# h = hashlib.sha1(b'something')
h = hashlib.sha1(testHash.encode())
h = h.hexdigest()
# print(h.hexdigest())

# APP LOGIC
# help menu
if len(sys.argv) < 2:
    print('')
    print('================================== help ==================================')
    print('')
    print('Usage :')
    print('p <account name> [attribute flag]    - search')
    print('p <"accounts" | "acc">               - list')
    print('p <"add">                            - add')
    print('p                                    - help')
    print('')
    print('Common Attribute Flags :')
    for account in ACCOUNTS:
        for key in ACCOUNTS[account]:
            print('[' + key[:1] + ']' + key[1:])
        break
    print('')
    sys.exit()

# getting inputs, init values (https://stackoverflow.com/questions/5423381/checking-if-sys-argvx-is-defined)
# first arg is always the script name
arguments = ['script_name','account_name', 'attribute_flag']
args = dict(zip(arguments, sys.argv))
for arg in args:
    args[arg] = args[arg].lower()
    # args[arg] = args[arg].encode('utf-8').strip()
fail = True

if args['account_name'] == 'add':
    fail = False
    # add a ACCOUNTS record
    # need to read and write from a file to keep data / changes
    application = input('application : ')
    user = input('user name : ')
    password = input('password : ')
    notes = input('notes : ')

    key = get_key(application)

    encrypted_password = encrypt(password, key)
    print('encrypted_password : ' + encrypted_password)
    decrypted = decrypt(encrypted_password, key)
    print('decrypted : ' + decrypted)

    ACCOUNTS[application] = {
        'user': user,
        'password': encrypted_password,
        'notes': notes
        }
    new_json = json.dumps(ACCOUNTS, indent=4, separators=(',', ': '))

    with open(login_path, 'w') as data_file_write:
        data_file_write.write(new_json)
    # print(ACCOUNTS) # working

elif args['account_name'] in ACCOUNT_LABELS and len(sys.argv) < 3:
    print('')
    print('Current accounts :')
    for acc in list(ACCOUNTS):
        print(acc)
    print('')
    sys.exit()

else:
    for acc in list(ACCOUNTS):
        if args['account_name'] == acc:
            fail = False
            if 'attribute_flag' in args:
                if args['attribute_flag'] == 'u':
                    pyperclip.copy(ACCOUNTS[acc]['user'])
                    print('')
                    print('user for ' + args['account_name'] + ' copied to clip board')
                    print('')
                    break
                elif args['attribute_flag'] == 'p':
                    password = ACCOUNTS[acc]['password']
                    # password = password.encode('utf-8').strip()
                    print('password : ')
                    print(password)
                    key = get_key(acc)
                    decrypted = decrypt(password, key)
                    pyperclip.copy(decrypted)
                    print('')
                    print('password for ' + args['account_name'] + ' copied to clip board')
                    print('')
                    break
                elif args['attribute_flag'] == 'n':
                    # pyperclip.copy(ACCOUNTS[acc]['notes'])
                    # print('notes for ' + args['account_name'] + ' copied to clip board')
                    print('')
                    print("Notes for " + acc + " :")
                    print(ACCOUNTS[acc]['notes'])
                    print('')
                    break
                elif args['attribute_flag'] == 'show':
                    print('')
                    print('[ ' + acc + ' ]')
                    for key in ACCOUNTS[acc]:
                        print('[' + key[:1] + ']' + key[1:] + ' : ' + ACCOUNTS[acc][key])
                    print('')
                    break
                else:
                    print('invalid option flag')
                    break
            else:
                print('')
                print('Available attribute flags for ' + args['account_name'] + ' :')
                for key in ACCOUNTS[acc]:
                    print('[' + key[:1] + ']' + key[1:])
                print('')
                break

if fail == True:
    print('')
    print('!!! There is no account named ' + args['account_name'] + ' !!!')
    print('')
    print('Please run the program with no arguments to see the help menu')
    print('')
