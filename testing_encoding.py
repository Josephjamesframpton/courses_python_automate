#!/bin/bash

# this example :
# reads an encripted string from a file
# parses to json
# decrypts
# concats new txt
# encrypts again
# writes to a file
# then reads, parses and displays the new hash to confirm

# NOTE check differences between this script and the main login function
# all of this is working (parsing, encrypting, decrypting, parsing back to json and back to python)

import sys, pyperclip, hashlib, json, base64
from Crypto import Random
from Crypto.Cipher import AES

key = 'abcdefghijklmnop'
login_path = './test_encode.txt'

def encrypt(data, key):
    iv_aes = Random.new().read(AES.block_size) # salt?
    # print('iv_aes : ')
    # print(iv_aes)
    cipher_aes = AES.new(key.encode(), AES.MODE_CFB, iv_aes) # key
    # print('cipher_aes : ')
    # print(cipher_aes)
    encrypted_aes = cipher_aes.encrypt(data.encode()) # encription
    result = base64.b64encode(iv_aes + encrypted_aes).decode('utf-8')
    # print(result)
    return result

def decrypt(encrypted_aes, key):
    # print('result passed : ')
    # print(encrypted_aes)
    decoded_base64 = base64.b64decode(encrypted_aes)
    iv_aes = decoded_base64[:AES.block_size]
    # print('iv_aes : ')
    # print(iv_aes)
    dec_cipher_aes = AES.new(key.encode(), AES.MODE_CFB, iv_aes)
    # print('dec_cipher_aes : ')
    # print(dec_cipher_aes)
    return dec_cipher_aes.decrypt(decoded_base64[AES.block_size:]).decode('utf-8')

# with open(login_path, 'r') as data_file:
#     data = json.load(data_file)

# print('reading/parsing from file')
# string = data['simple']
# print(string)
# string = decrypt(string, key)
# print(string)
# string += 'next'
# print(string)
string = sys.argv[1]
print("input string :" + string)

encrypted_password = encrypt(string, key)
print('new encrypted password : ' + encrypted_password)
decrypted = decrypt(encrypted_password, key)
print('decrypted from python: ' + decrypted)

# obj = '{"simple": "' + encrypted_password + '"}'
# not working:
# data['simple'] = encrypted_password
# obj = json.dumps(data.__dict__)
# obj = json.dumps(obj, default=lambda o: o.__dict__)

data = {}
data['simple'] = encrypted_password
obj = json.dumps(data, indent=4, separators=(',', ': '))

print('JSON parsed object to save : ' + obj)

with open(login_path, 'w') as data_file_write:
    data_file_write.write(obj)
print('wrote obj to file')

with open(login_path, 'r') as data_file:
    data = json.load(data_file)
print('reading from file')
print('parsing to JSON')

print("Full data obj from file (parsed) :")
print(data)
print("value from obj :")
print(data['simple'])

decrypted = decrypt(encrypted_password, key)
print("decrypted from file after parse :")
print(decrypted)

pyperclip.copy(decrypted)
