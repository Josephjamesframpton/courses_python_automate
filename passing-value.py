def eggs(param):
    param + 'hello'

spam = "thisisastring"
eggs(spam)
print(spam)

# this doesnt work as it is passed by value
# this creates a new value when passed
# opposed to passing a reference to data ( when data is changed
# all references to it are updated)

# arrays are passed by reference but strings are passed by value

# string is an immutatable type where as a list type is mutatable

# :. string needs to be returned in a function

def eggs1(param1):
    return param1 + 'hello'

spam1 = "thisisastring"
spam1 = eggs1(spam1)
print(spam1)

# using
# import copy
# var1 = copy.copy(var0)
# will allow you to create two independant copies of a list
# not just a simple copy of a reference
