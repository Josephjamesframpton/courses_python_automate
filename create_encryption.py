#!/bin/bash

import sys, pyperclip, hashlib, json, base64
from Crypto import Random
from Crypto.Cipher import AES

key = '1234567890123456'

def encrypt(data, key):
    iv_aes = Random.new().read(AES.block_size) # salt?
    # print('iv_aes : ')
    # print(iv_aes)
    cipher_aes = AES.new(key.encode(), AES.MODE_CFB, iv_aes) # key
    # print('cipher_aes : ')
    # print(cipher_aes)
    encrypted_aes = cipher_aes.encrypt(data.encode()) # encription
    result = base64.b64encode(iv_aes + encrypted_aes).decode('utf-8')
    # print(result)
    return result

input_string = sys.argv[1]

print(input_string)

input_string = encrypt(input_string, key)

print(input_string)
